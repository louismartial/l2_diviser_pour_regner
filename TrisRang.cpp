#include <cstdlib>
#include <cmath>
#include "TrisRang.h"

#include <iostream>

using namespace std;

void fusion(int n1, int n2, int* T1, int* T2, int* T)
{
  int i1 = 0, i2 = 0;
  for(int is = 0; is < n1 + n2; is++)
  {
    if(i2 >= n2 || (i1 < n1 && T1[i1] <= T2[i2]))
    {
      T[is] = T1[i1];
      i1++;
    }
    else
    {
      T[is] = T2[i2];
      i2++;
    }
  }
}

void trifusion(int n, int* T)
{
  if (n > 1)
  {
    int n1 = (n + 1) / 2;
    int n2 = n / 2;
    int *T1 = new int[n1];
    int *T2 = new int[n2];
    for(int i = 0; i < n; i++)
    {
      T1[i/2] = T[i];
      i++;
      if(i == n)
        break;
      T2[i/2] = T[i];
    }
    trifusion(n1, T1);
    trifusion(n2, T2);
    fusion(n1, n2, T1, T2, T);
    delete[] T1;
    delete[] T2;
  } 
}

int pivot(int n, int* T, bool b) 
{
  return (b)?T[rand()%n]:T[0];
}

int rang(int k, int n, int* T, bool b)
{
  if(n == 1)
    return T[0];
  int p = pivot(n, T, b);
  int nInf = 0, nEq = 0;
  for(int i = 0; i < n; i++)
  {
    if(T[i] < p)
      nInf++;
    else if(T[i] == p)
      nEq++;
  }
  if (k <= nInf)
  {
    int *TInf = new int[nInf];
    int j = 0;
    for(int i = 0; i < n; i++)
    {
      if(T[i] < p)
      {
        TInf[j] = T[i];
        j++;
      }
    }
    return rang(k, nInf, TInf, b);
  }
  else if(nInf < k && k <= nInf + nEq)
  {
    return p;
  }
  else
  {
    int nSup = n - nInf - nEq;
    int *TSup = new int[nSup];
    int j = 0;
    for(int i = 0; i < n; i++)
    {
      if(T[i] > p)
      {
        TSup[j] = T[i];
        j++;
      }
    }
    return rang(k - nInf - nEq, nSup, TSup, b);
  }
}

void echange(int *T, int i, int j) {
  int temp = T[i];
  T[i] = T[j];
  T[j] = temp;
}

int coupe(int *T, int debut, int fin, bool b)
{
  int pivotValeur; // !!
  if (b) {
    if (debut = 0)
      pivotValeur = pivot(fin - debut, T, true);
    else
      pivotValeur = pivot(fin - debut + 1, T, true);
  }
  else {
    pivotValeur = T[debut]; // car T[0] (qui est renvoyé par la fonction pivot) peut ne pas exister…
  }
  int pivotIndice;
  for(int i = debut; i <= fin; i++)
  {
      if (T[i] == pivotValeur)
      {
          pivotIndice = i;
          break;
      }
  }
  echange(T, pivotIndice, fin);

  pivotValeur = T[fin];
  pivotIndice = fin;
  int i = debut - 1;
  for(int j = debut; j <= fin - 1; j++)
  {
    if(T[j] <= pivotValeur)
    {
      i++;
      echange(T, i, j);
    }
    
  }
  echange(T, i + 1, fin);
  return i + 1;
}

void triRapideAvecBorne(int * T, int debut, int fin, bool b)
{
  if(debut < fin)
  {
    int pivotIndice = coupe(T, debut, fin, b);
    triRapideAvecBorne(T, debut, pivotIndice - 1, b);
    triRapideAvecBorne(T, pivotIndice + 1, fin, b);
  }
}

void trirapide(int n, int* T, bool b)
{
  triRapideAvecBorne(T, 0, n - 1, b);
}